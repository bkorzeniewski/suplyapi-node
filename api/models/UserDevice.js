module.exports = {
  autoCreatedAt: false,
  autoUpdatedAt: false,
  tableName: 'user_device',

  attributes: {
    user: {
      model: 'user'
    },
    device: {
      model: 'device'
    }
  }
}
