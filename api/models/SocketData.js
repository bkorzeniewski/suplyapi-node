module.exports = {

  autoCreatedAt: false,
  autoUpdatedAt: false,
  tableName: 'socket_data',

  attributes: {
    datatime_at: {type: 'datetime'},
    ampere: {type: 'float'},
    volt: {type: 'float'},
    state: {type: 'boolean'},
    socket:{
      model: 'socket'
    }
  }
};
