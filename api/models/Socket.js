module.exports = {

  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes: {
    name: {type: 'string'},
    color: {type: 'string'},
    device:{
      model: 'device'
    },
    socketsData:{
      collection: 'socketdata',
      via: 'socket'
    },
    socketsTimetable:{
      collection: 'sockettimetable',
      via: 'socket'
    }
  }
};
