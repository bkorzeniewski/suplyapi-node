module.exports = {

  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes: {
    username: {type: 'string'},
    email: {type: 'string'},
    password: {type: 'string'},
    mqtt_client: {
      model:'mqttclient'
    },
    devices: {
      collection: 'device',
      via: 'users',
      through: 'userdevice'
    }
  }
};
