module.exports = {

  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes: {
    name: {type: 'string'},
    mqtt_client: {
      model:'mqttClient'
    },
    users:{
      collection: 'user',
      via: 'devices',
      through: 'userdevice'
    },
    sockets:{
      collection: 'socket',
      via: 'device'
    }
  }
};
