module.exports = {

  autoCreatedAt: false,
  autoUpdatedAt: false,
  tableName: 'socket_timetable',

  attributes: {
    datatime_start: {type: 'datetime'},
    datatime_end: {type: 'datetime'},
    state: {type: 'boolean'},
    socket:{
      model: 'socket'
    }
  }
};
