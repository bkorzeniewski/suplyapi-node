module.exports = {

  autoCreatedAt: false,
  autoUpdatedAt: false,
  tableName: 'mqtt_client',

  attributes: {
    password: {type: 'string'},
    api_key: {type: 'string'},
    user: {
      model: 'user'
    },
    device: {
      model: 'device'
    },
    mqttAcls:{
      collection: 'mqttacl',
      via: 'mqttClient'
    }
  }
};
