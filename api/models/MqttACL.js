module.exports = {

  autoCreatedAt: false,
  autoUpdatedAt: false,
  tableName: 'mqtt_acl',

  attributes: {
    topic: {type: 'string'},
    access: {type: 'integer'},
    mqttClient:{
      model: 'mqttclient'
    }
  }

};
